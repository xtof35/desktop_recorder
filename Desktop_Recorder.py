#from tkinter import *
from tkinter.messagebox import *
from tkinter.filedialog import *
import pathlib
import subprocess
import os


class MainWindow():
    #----------------
    def __init__(self, main):

        # canvas for image
        self.canvas = Canvas(main, width=640, height=360)
        self.canvas.grid(row=5, column=0)

        # images
        self.image = PhotoImage(file="origine.png")
        self.image_on_canvas = self.canvas.create_image(0, 0, anchor = NW,  image = self.image )

        # button to change image
        self.button = Button(main, text="Preview", font=("georgia", 12), bg='white', fg='#333333', command=self.onButton)
        self.button.grid(row=4, column=0)

    #----------------

    def onButton(self):

        preview()
        # change image
        self.image = PhotoImage(file="Capture.png")
        self.canvas.itemconfig(self.image_on_canvas, image = self.image)

def browse3():
# save as
    filepath3 = asksaveasfilename(title="Enregistrer sous",filetypes = (("mp4 files","*.mp4"),("all files","*.*")))
    extension = pathlib.Path(filepath3).suffix

    if extension != '.mp4':
        filepath3=filepath3+'.mp4'
    output.delete(0, END)
    output.insert(0, filepath3)


def entry_change ():
    #selection des boutons radio

    entry_selection =radio_contact.get()
    if entry_selection == 1:
      codec = "libx264"
    elif entry_selection == 2:
      codec = "libx265"
    else:
      print ("selectionner un codec")



def affiche_popup(message):
        # creer une variable avec une instance de fenetre
        popup = Tk()
        # personnaliser la fenetre
        popup.title("Message d'alerte")
        popup.config(background='#505050')

        # affiche texte
        texte = Label(popup, text=message, font=("trebuchet", 12), bg='#505050', fg='white', pady=5, padx=10)
        texte.pack()

        # ajouter boutton OK
        button_enc = Button(popup, text="OK", font=("georgia", 12), bg='white', fg='#333333', pady=5, command=popup.destroy)
        button_enc.pack()

        # afficher la fenetre
        popup.mainloop()


def preview ():
    #Test presence de fichiers

    if width.get() == 0 or height.get() == 0:
        affiche_popup("La largeur et la hauteur doivent être supérieure à 0")
        return
    print (ffmpeg_path,  '-f', 'gdigrab',
       '-video_size', width.get() + 'x' + height.get(),
       '-offset_x', xoffset.get(),
       '-offset_y', yoffset.get(),
       '-show_region', '1',
       '-i', 'desktop',
 #      '-vf', 'scale=w=640:h=360:force_original_aspect_ratio=decrease,pad=640:360:(ow-iw)/2:0:black',
       '-vframes', '1',
       '-y',
       'Capture.png')

    subprocess.call(
      [ffmpeg_path,  '-f', 'gdigrab',
       '-video_size', width.get() + 'x' + height.get(),
       '-offset_x', xoffset.get(),
       '-offset_y', yoffset.get(),
       '-show_region', '1',
       '-i', 'desktop',
       '-vf', 'scale=w=640:h=360:force_original_aspect_ratio=decrease,pad=640:360:(ow-iw)/2:0:black',
       '-vframes', '1',
       '-y',
       'Capture.png'],
       shell=True)


def encode ():


    #Test valeur de débit
    valeurdebitv = int(debit.get())

    if type(valeurdebitv) != 'int':
        if valeurdebitv > 499:

            debitv = str(debit.get()) + "K"
        else:
            debit.delete(0, END)
            debit.insert(0, 0)
            affiche_popup("indiquez un débit supérieur à 500")
            return
    else:
        return
    #test definition
    if width.get() == 0 or height.get() == 0:
        affiche_popup("La largeur et la hauteur doivent être supérieure à 0")
        return

    #test duree
    if duree.get() == 0:
        affiche_popup("La durée doit être supérieure à 0")
        return

    #test fichier de sortie
    if output.get() == 0:
        affiche_popup("Donner un fichier de sortie")
        return

    #choix encodage
    entry_selection = radio_contact.get()
    if entry_selection == 1:
      codec = "libx264"
    elif entry_selection == 2:
      codec = "libx265"
    else:
      print("selectionner un codec")
      return

    print(ffmpeg_path,  '-f', 'gdigrab',
       '-video_size', width.get() + 'x' + height.get(),
       '-offset_x', xoffset.get(),
       '-offset_y', yoffset.get(),
       '-show_region', '1',
       '-r', rate.get(),
       '-i', 'desktop',
       '-t', duree.get(),
       '-c:v', codec,
       '-b', debitv,
       '-y',
       output.get())

    subprocess.call(
      [ffmpeg_path,  '-f', 'gdigrab',
       '-video_size', width.get() + 'x' + height.get(),
       '-offset_x', xoffset.get(),
       '-offset_y', yoffset.get(),
       '-show_region', '1',
       '-r', rate.get(),
       '-i', 'desktop',
       '-t', duree.get(),
       '-c:v', codec,
       '-b', debitv,
       '-y',
       output.get(),
       ],
       shell=True)
    affiche_popup("Encodage Terminé !")


#creer une variable avec une instance de fenetre
window = Tk()
# définition des variables
local_path = os.path.dirname(os.path.abspath("Desktop_Recorder.py"))
ffmpeg_path = local_path + r'\tools\ffmpeg.exe'



#personnaliser la fenetre
window.title("Desktop Recorder")
window.geometry("654x850")
window.resizable(False, False)
window.iconbitmap("icone.ico")
window.config(background='#505050')

#creation image
largeur = 100
hauteur = 100
image = PhotoImage(file="logo.png")
canvas = Canvas(window, width=largeur, height=hauteur, bg='#505050', bd=0, highlightthickness=0)
canvas.create_image(largeur/2, hauteur/2, image=image)
canvas.grid(row=1, sticky='n')

#creation de frame parametre de capture

capture = LabelFrame(window, text="Fenêtre de capture", padx=10, pady=10, bg='#505050', fg='white')
capture.grid(row=2, sticky='ew')


#creer un champ width
# texte
txtwidth = Label(capture, text="Largeur :", font=("georgia", 12), bg='#505050', fg='white')
txtwidth.grid(row=1, column=0, sticky="w", pady=10)
nombre = window.register(lambda s: not s or s.isdigit())
width = Entry(capture, font=("trebuchet", 14), bg='#878787', fg='white', validate='key', validatecommand=(nombre, '%P'))
width.insert(0, "1280")
width.grid(row=1, column=1, sticky='ew', columnspan=3)

#creer un champ height
# texte
txtheight = Label(capture, text="Hauteur :", font=("georgia", 12), bg='#505050', fg='white')
txtheight.grid(row=2, column=0, sticky="w", pady=10)
height = Entry(capture, font=("trebuchet", 14), bg='#878787', fg='white', validate='key', validatecommand=(nombre, '%P'))
height.insert(0, "720")
height.grid(row=2, column=1, sticky='ew', columnspan=3)

#creer un champ X_offset
# texte
txtxoffset = Label(capture, text="Offset en X :", font=("georgia", 12), bg='#505050', fg='white')
txtxoffset.grid(row=1, column=4, sticky="w", pady=10)
xoffset = Entry(capture, font=("trebuchet", 14), bg='#878787', fg='white', validate='key', validatecommand=(nombre, '%P'))
xoffset.insert(0, "0")
xoffset.grid(row=1, column=5, sticky='ew', columnspan=3)


#creer un champ Y ffset
# texte
txtyoffset = Label(capture, text="Offset en Y :", font=("georgia", 12), bg='#505050', fg='white')
txtyoffset.grid(row=2, column=4, sticky="w", pady=10)
yoffset = Entry(capture, font=("trebuchet", 14), bg='#878787', fg='white', validate='key', validatecommand=(nombre, '%P'))
yoffset.insert(0, "0")
yoffset.grid(row=2, column=5, sticky='ew', columnspan=3)

#creer un champ durée
# texte
txtduree = Label(capture, text="Durée (s) :", font=("georgia", 12), bg='#505050', fg='white')
txtduree.grid(row=3, column=0, sticky="w", pady=10)
duree = Entry(capture, font=("trebuchet", 14), bg='#878787', fg='white', validate='key', validatecommand=(nombre, '%P'))
duree.insert(0, "10")
duree.grid(row=3, column=1, sticky='ew', columnspan=3)

#creer un champ framerate
# texte
txtrate = Label(capture, text="images/s :", font=("georgia", 12), bg='#505050', fg='white')
txtrate.grid(row=3, column=4, sticky="w", pady=10)
rate = Entry(capture, font=("trebuchet", 14), bg='#878787', fg='white', validate='key', validatecommand=(nombre, '%P'))
rate.insert(0, "25")
rate.grid(row=3, column=5, sticky='ew', columnspan=3)

MainWindow(window)


#creation de frame paramètres encodage
encodage = LabelFrame(window, text="Paramètres d'encodage", padx=10, pady=10, bg='#505050', fg='white')
encodage.grid(row=8, column=0, sticky='ew')

encodage.grid_columnconfigure(8, weight=1)

#bouton radio selection encodage
label_title = Label(encodage, text="Codec :", font=("georgia", 12), bg='#505050', fg='white')
label_title.grid(row=1, column=0, sticky="w")
radio_contact = IntVar()
radiocontact1 = Radiobutton(encodage, text='H264', variable=radio_contact, indicatoron=0, value=1, command=entry_change, padx=5, pady=5, bg='#878787')
radiocontact1.grid(row=1, column=1, sticky='n', padx=10)
radiocontact1.select()

radiocontact2 = Radiobutton(encodage, text='H265', variable=radio_contact, indicatoron=0, value=2, command=entry_change, padx=5, pady=5, bg='#878787')
radiocontact2.grid(row=1, column=2, sticky='n', padx=10)
#creer un champ débit
# texte
label_title = Label(encodage, text="Débit en Kbits/s :", font=("georgia", 12), bg='#505050', fg='white')
label_title.grid(row=1, column=4, sticky="w", pady=10)
nombre = window.register(lambda s: not s or s.isdigit())
debit = Entry(encodage, font=("trebuchet", 14), bg='#878787', fg='white', validate='key', validatecommand=(nombre, '%P'))
debit.insert(0, "0")
debit.grid(row=1, column=5, columnspan=3, sticky='ew', padx= 10)

# creer un champ Output
output = Entry(encodage, font=("trebuchet", 12), bg='#878787', width=60, fg='white')
output.grid(row=2, column=0, sticky='w', columnspan=7, padx=10)
# ajouter boutton3
button_out = Button(encodage, text="Output", font=("georgia", 12), bg='white', fg='#333333', command=browse3)
button_out.grid(row=2, column=7, sticky="e")



# ajouter boutton encode
button_enc = Button(window, text="Encode", font=("georgia", 16), bg='white', fg='#333333', command=encode)
button_enc.grid(row=10, sticky='n', pady=5)


#afficher la fenetre
window.mainloop()
